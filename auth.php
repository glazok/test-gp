<?php

$subdomain = '777belka777'; //Поддомен нужного аккаунта
$link = 'https://' . $subdomain . '.amocrm.ru/oauth2/access_token'; //Формируем URL для запроса

/** Соберем данные для запроса */
$data = [
	'client_id' => 'e9d84abc-e8a0-423f-a330-0fa8e4552047', // id нашей интеграции
	'client_secret' => 'pHBwNi1X531PKuyR0ooJGOUTLYR1bxVMzCdJoHjdlJU4JkF6YKLcH53dJ5EHhF5w', // секретный ключ нашей интеграции
	'grant_type' => 'authorization_code',
	'code' => 'def50200cbbe365ea645b23a83dc3a088cc009c3490730f38240782c8c9fdbb6887b10945ed6e30deb825e3541fcf2a3eb65311c6dda314cf7d36568cc338fccbcc5397e13e42712c1e0b63501c615efba50c146d1a14def94098e20e7538bc9c492cdd95d593d4e781c19578bbc7769d55021397837ed33eaf11e94acb5a9512e227cbe6c09bf8830585d913d269a24077494b3358af69a78fa3d408fced17d7dc44ca5e363ff6983efc32d2f25f2581e3a64cc68118088ef5def59219a76af088608da45f701ae9ed3f3121e86218c1fb73c34a32aa41c734d038890839484be6a50287fc54ced361560f87e9cefbbaceab7239e22cd07eae352c81d32fa7f10151af8c7959d3328dbb85e54ede7583fd2500e5bb9b5dccd3be173ba198f8afd54914fab7bcc7743122fe4f0056c0c98c5d68afa108f72ef23cf6b6e57f35a12f12721431aa15068e17dae02a8c8c6ef2abd5b2c32f696b96ba3f32d4ef73f84b6d1bde22a38ea7e4ffdc5db82dffa5d96f1439ef692c298d126c720d13a0755cd74676cca0a238a20478ae45962f7546d0d56adc5507b1a43d4411d01ab9be826be1f1bf866fff19bf44853f4cabf03f201c37ed14636c6c72942e81a856524670f', // код авторизации нашей интеграции
	'redirect_uri' => 'http://tuhar.pahatrop.ru/',// домен сайта нашей интеграции
];

/**
 * Нам необходимо инициировать запрос к серверу.
 * Воспользуемся библиотекой cURL (поставляется в составе PHP).
 * Вы также можете использовать и кроссплатформенную программу cURL, если вы не программируете на PHP.
 */
$curl = curl_init(); //Сохраняем дескриптор сеанса cURL
/** Устанавливаем необходимые опции для сеанса cURL  */
curl_setopt($curl,CURLOPT_RETURNTRANSFER, true);
curl_setopt($curl,CURLOPT_USERAGENT,'amoCRM-oAuth-client/1.0');
curl_setopt($curl,CURLOPT_URL, $link);
curl_setopt($curl,CURLOPT_HTTPHEADER,['Content-Type:application/json']);
curl_setopt($curl,CURLOPT_HEADER, false);
curl_setopt($curl,CURLOPT_CUSTOMREQUEST, 'POST');
curl_setopt($curl,CURLOPT_POSTFIELDS, json_encode($data));
curl_setopt($curl,CURLOPT_SSL_VERIFYPEER, 1);
curl_setopt($curl,CURLOPT_SSL_VERIFYHOST, 2);
$out = curl_exec($curl); //Инициируем запрос к API и сохраняем ответ в переменную
$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
curl_close($curl);
/** Теперь мы можем обработать ответ, полученный от сервера. Это пример. Вы можете обработать данные своим способом. */
$code = (int)$code;

// коды возможных ошибок
$errors = [
	400 => 'Bad request',
	401 => 'Unauthorized',
	403 => 'Forbidden',
	404 => 'Not found',
	500 => 'Internal server error',
	502 => 'Bad gateway',
	503 => 'Service unavailable',
];

try
{
	/** Если код ответа не успешный - возвращаем сообщение об ошибке  */
	if ($code < 200 || $code > 204) {
		throw new Exception(isset($errors[$code]) ? $errors[$code] : 'Undefined error', $code);
	}
}
catch(\Exception $e)
{
	die('Ошибка: ' . $e->getMessage() . PHP_EOL . 'Код ошибки: ' . $e->getCode());
}

/**
 * Данные получаем в формате JSON, поэтому, для получения читаемых данных,
 * нам придётся перевести ответ в формат, понятный PHP
 */
$response = json_decode($out, true);

$access_token = $response['access_token']; //Access токен
$refresh_token = $response['refresh_token']; //Refresh токен
$token_type = $response['token_type']; //Тип токена
$expires_in = $response['expires_in']; //Через сколько действие токена истекает

// выведем наши токены. Скопируйте их для дальнейшего использования
// access_token будет использоваться для каждого запроса как идентификатор интеграции
var_dump($access_token);
var_dump($refresh_token );